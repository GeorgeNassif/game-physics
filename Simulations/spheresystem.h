#ifndef SPHERESYSTEM_H
#define SPHERESYSTEM_H

#include <vector>
#include <functional>

#include "util/vectorbase.h"
#include "util/vector4d.h"
using namespace GamePhysics;

class SphereSystem
{
public:
	struct Sphere
	{
		Vec3 pos;
		Vec3 vel;
	};	

	SphereSystem(int n, float radius);

	int AddPoint(const Vec3& pos, const Vec3& vel = Vec3(0,0,0))
	{
		Sphere s;
		s.pos = pos;
		s.vel = vel;
		m_spheres.push_back(s);

		return (int)m_spheres.size()-1;
	}

	void SetMass      (float mass      ) { m_mass       = mass;       }
	void SetRadius    (float radius    ) { m_radius     = radius;     }
	void SetForceScale(float forceScale) { m_forceScale = forceScale; }
	void SetDamping   (float damping   ) {m_damping     = damping;    }

	void SetKernel    (const std::function<float(float)>& kernel) { m_kernel = kernel; }
    
    void SetGravity  (const Vec3& gravity) {m_gravity = gravity;}

	const std::vector<Sphere>&  GetSpheres()  {return m_spheres; }

	void AdvanceEuler   (float dt, int iAccelerator, int iMaxSpheresPerLeaf);
	void AdvanceLeapFrog(float dt, int iAccelerator, int iMaxSpheresPerLeaf);

	void clear();
	void setNumberOfParticlesAndRadius(int n, float radius);
private:
	std::vector<Vec3> ComputeForces();
	std::vector<Vec3> ComputeForcesWithGrid();
	std::vector<Vec3> ComputeForcesWithKdTree();
    void ComputePairForce(int i, int j, std::vector<Vec3>& forces);
	void UpdatePositions(float dt);
	void UpdateVelocities(float dt, const std::vector<Vec3>& forces);

	std::vector<Sphere>  m_spheres;

	float m_mass;
    float m_radius;
    float m_forceScale;
    float m_damping;

    std::function<float(float)> m_kernel;

	Vec3 m_gravity;
};


#endif