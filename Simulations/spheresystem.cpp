#include "spheresystem.h"

#include <random>
#include <iostream>
#include <set>
#include <unordered_set>
#include <algorithm>


class KdNode
{
public:
    typedef const SphereSystem::Sphere* PSphere;
    typedef std::set<std::pair<PSphere,PSphere>> PairMap;

    //struct Hash {
    //    size_t operator()(const std::pair<PSphere,PSphere>& a) const {
    //        return std::hash<size_t>()((size_t)a.first ^ (size_t)a.second);
    //    }
    //};
    //typedef std::unordered_set<std::pair<PSphere,PSphere>,Hash> PairMap;

    static int   ms_MaxSpheresPerLeaf;
    static float ms_Radius;

public:
    virtual ~KdNode() {}

    virtual KdNode* AddSphere(PSphere s) = 0;
    virtual void ResolveCollisions(const std::function<void(PSphere,PSphere)>& onCollision, PairMap& finishedPairs) = 0;
    virtual void TreeStats(int& minHeight, int& maxHeight, int& minLeafsize, int& maxLeafsize, int currentHeight) = 0;
};

int   KdNode::ms_MaxSpheresPerLeaf = 10;
float KdNode::ms_Radius = 1.0f;

class KdSplit : public KdNode
{
private:
	Vec4 m_plane;
    KdNode*  m_children[2];

public:
    virtual ~KdSplit() { delete m_children[0]; delete m_children[1]; }
    
    KdSplit(const Vec4& plane, KdNode* lChild, KdNode* rChild)
        : m_plane(plane)
    {
        m_children[0] = lChild;
        m_children[1] = rChild;
    }

    virtual KdNode* AddSphere(PSphere s)
    {
		//vectorOut.x = P.x * V.x + P.y * V.y + P.z * V.z + P.w * 1.0f;
		float value = m_plane.x * s->pos.x + m_plane.y * s->pos.y + m_plane.z * s->pos.z + m_plane.T;
        bool left  = value <  ms_Radius;
        bool right = value > -ms_Radius;

        if (left ) m_children[0] = m_children[0]->AddSphere(s);
        if (right) m_children[1] = m_children[1]->AddSphere(s);

        return this;
    }

    virtual void ResolveCollisions(const std::function<void(PSphere,PSphere)>& onCollision, PairMap& finishedPairs)
    {
        m_children[0]->ResolveCollisions(onCollision, finishedPairs);
        m_children[1]->ResolveCollisions(onCollision, finishedPairs);
    }

    virtual void TreeStats(int& minHeight, int& maxHeight, int& minLeafsize, int& maxLeafsize, int currentHeight)
    {
        m_children[0]->TreeStats(minHeight, maxHeight, minLeafsize, maxLeafsize, currentHeight + 1);
        m_children[1]->TreeStats(minHeight, maxHeight, minLeafsize, maxLeafsize, currentHeight + 1);
    }
};

class KdLeaf : public KdNode
{
private:
    std::vector<PSphere> m_spheres;    

public:
    virtual KdNode* AddSphere(PSphere s)
    {
        m_spheres.push_back(s);

        if (m_spheres.size() > ms_MaxSpheresPerLeaf)
        {
            // Determine bounding box of spheres
			float val = std::numeric_limits<float>::max();
			Vec3 min = Vec3(val, val, val);
			Vec3 max = -min;
            for (auto s : m_spheres)
            {
			
				min.x = (min.x < s->pos.x) ? min.x : s->pos.x;
				min.y = (min.y < s->pos.y) ? min.y : s->pos.y;
				min.z = (min.z < s->pos.z) ? min.z : s->pos.z;
				
				max.x = (max.x > s->pos.x) ? max.x : s->pos.x;
				max.y = (max.y > s->pos.y) ? max.y : s->pos.y;
				max.z = (max.z > s->pos.z) ? max.z : s->pos.z;

            }
            min -= Vec3(ms_Radius);
            max += Vec3(ms_Radius);
			Vec3 extent = max - min;

            // Choose best split axis
            int axis = -1;
            int maxscore = -1;

            for (int a = 0; a < 3; a++)
            {
				Vec3 normal(0.0f);
				normal[a] = 1.0f;
               /*
				Result.x = Normal.x;
				Result.y = Normal.y;
				Result.z = Normal.z;
				Result.w = -(Point.x * Normal.x + Point.y * Normal.y + Point.z * Normal.z);

			   */

				Vec3 point = 0.5f * (min + max);
				float t = -1*dot(point, normal);
				Vec4 plane(normal.x, normal.y, normal.z, t);

                int score = (int)(2 * m_spheres.size());
                
                for (auto s : m_spheres)
                {
					float value = plane.x * s->pos.x + plane.y * s->pos.y + plane.z * s->pos.z + plane.T;
					bool left = value <  ms_Radius;
					bool right = value > -ms_Radius;

                    if (left)  score--;
                    if (right) score--;
                }
                
                if (score > maxscore)
                {
                    axis = a;
                    maxscore = score;
                }
            }

            if (maxscore > 0) 
            {
				Vec3 normal(0.0f);
				normal[axis] = 1.0f;
				/*
				Result.x = Normal.x;
				Result.y = Normal.y;
				Result.z = Normal.z;
				Result.w = -(Point.x * Normal.x + Point.y * Normal.y + Point.z * Normal.z);

				*/

				Vec3 point = 0.5f * (min + max);
				float t = -1 * dot(point, normal);
				Vec4 plane(normal.x, normal.y, normal.z, t);


                std::vector<PSphere> spheresLeft, spheresRight;
                for (auto s : m_spheres)
                {
					float value = plane.x * s->pos.x + plane.y * s->pos.y + plane.z * s->pos.z + plane.T;
					bool left = value <  ms_Radius;
					bool right = value > -ms_Radius;

                    if (left)  spheresLeft .push_back(s);
                    if (right) spheresRight.push_back(s);
                }

                KdLeaf* lChild = this;
                lChild->m_spheres = std::move(spheresLeft);

                KdLeaf* rChild = new KdLeaf();
                rChild->m_spheres = std::move(spheresRight);

                return new KdSplit(plane, lChild, rChild);
            }
            else
            {
                std::cout << "WARNING: Leaf overflow" << std::endl;
            }
        }

        return this;
    }

    virtual void ResolveCollisions(const std::function<void(PSphere,PSphere)>& onCollision, PairMap& finishedPairs)
    {
        for (int i = 0; i < m_spheres.size(); i++)
        {
            for (int j = 0; j < i; j++)
            {
                PSphere si = m_spheres[i];
                PSphere sj = m_spheres[j];

                std::pair<PSphere,PSphere> pair(si, sj);
                if (si > sj) { PSphere tmp = pair.first; pair.first = pair.second; pair.second = tmp; } 
                
                if (finishedPairs.insert(pair).second)
                {
                    onCollision(si, sj);
                }
            }
        }
    }

    virtual void TreeStats(int& minHeight, int& maxHeight, int& minLeafsize, int& maxLeafsize, int currentHeight)
    {
        minHeight = std::min(minHeight, currentHeight);
        maxHeight = std::max(maxHeight, currentHeight);
        minLeafsize = std::min(minLeafsize, (int)m_spheres.size());
        maxLeafsize = std::max(maxLeafsize, (int)m_spheres.size());
    }
};



SphereSystem::SphereSystem(int n, float radius)
 :  m_mass(1.0f),
    m_radius(radius),
    m_forceScale(1.0f),
    m_damping(1.0f)
{
	m_gravity = Vec3(0,0,0);

    std::mt19937 eng;
    std::uniform_real_distribution<float> jitter(-0.01f*m_radius, 0.01f*m_radius);

    int m = (int)std::ceil(std::pow((float)n, 1.0f/3.0f));

    for (int z = 0; z < m; z++)
    for (int y = 0; y < m; y++)
    for (int x = 0; x < m; x++)
    {
        if ((z*m+y)*m+x < n)
        {
            Vec3 pos(-0.5f + m_radius + x*2.0f*m_radius + jitter(eng), 
                         -0.5f + m_radius + y*2.0f*m_radius + jitter(eng),
                         -0.5f + m_radius + z*2.0f*m_radius + jitter(eng));
            AddPoint(pos);
        }
    }

    //std::mt19937 eng;
    //std::uniform_real_distribution<float> rand(-0.5f + m_radius, 0.5f - m_radius);
    //for (int i = 0; i < n; i++)
    //{
    //    XMFLOAT3 pos(rand(eng), rand(eng), rand(eng));
    //
    //    AddPoint(pos);
    //}

    m_kernel = [](float x){ return 1 - x; };
}


void SphereSystem::clear() {
	m_spheres.clear();
}
void SphereSystem::setNumberOfParticlesAndRadius(int n, float radius)
{
	std::mt19937 eng;
	std::uniform_real_distribution<float> jitter(-0.01f*m_radius, 0.01f*m_radius);
	int m = (int)std::ceil(std::pow((float)n, 1.0f / 3.0f));

	for (int z = 0; z < m; z++)
		for (int y = 0; y < m; y++)
			for (int x = 0; x < m; x++)
			{
				if ((z*m + y)*m + x < n)
				{
					Vec3 pos(-0.5f + m_radius + x*2.0f*m_radius + jitter(eng),
						-0.5f + m_radius + y*2.0f*m_radius + jitter(eng),
						-0.5f + m_radius + z*2.0f*m_radius + jitter(eng));
					AddPoint(pos);
				}
			}
}

void SphereSystem::AdvanceEuler(float dt, int iAccelerator, int iMaxSpheresPerLeaf)
{
    KdNode::ms_Radius = m_radius;
    KdNode::ms_MaxSpheresPerLeaf = iMaxSpheresPerLeaf;

    std::vector<Vec3> forces;

    switch (iAccelerator)
    {
    case 0: forces = ComputeForces(); break;
    case 1: forces = ComputeForcesWithGrid(); break;
    case 2: forces = ComputeForcesWithKdTree(); break;
    }

	UpdatePositions(dt);

	UpdateVelocities(dt, forces);
}

void SphereSystem::AdvanceLeapFrog(float dt, int iAccelerator, int iMaxSpheresPerLeaf)
{
    KdNode::ms_Radius = m_radius;
    KdNode::ms_MaxSpheresPerLeaf = iMaxSpheresPerLeaf;

    std::vector<Vec3> forces;
    
    switch (iAccelerator)
    {
    case 0: forces = ComputeForces(); break;
    case 1: forces = ComputeForcesWithGrid(); break;
    case 2: forces = ComputeForcesWithKdTree(); break;
    }
	
	UpdateVelocities(dt, forces);
	
    UpdatePositions(dt);
}


std::vector<Vec3> SphereSystem::ComputeForces()
{
	// Gravity forces
	Vec3 gravity = m_gravity;

	std::vector<Vec3> forces(m_spheres.size(), gravity * m_mass);

    // Penalty forces
    for (int i = 0; i < (int)m_spheres.size(); i++)
    {
        for (int j = 0; j < i; j++)
        {            
            ComputePairForce(i, j, forces);
        }
    }

	// Damping forces
	for (size_t i=0; i<m_spheres.size(); i++)
	{
		Vec3 vel = m_spheres[i].vel;
		forces[i] += -m_damping * vel;
	}

	return forces;
}

std::vector<Vec3> SphereSystem::ComputeForcesWithGrid()
{
    float gridspacing = 2.0f * m_radius;
    int gridsize = (int) std::ceil(1.0f / gridspacing + 1E-3f);
    int gridcells = gridsize * gridsize * gridsize;
    static int bucketsize = 10;
    static std::vector<int> counters;
    static std::vector<int> buckets;
    static std::vector<int> usedBuckets;
    counters.resize(gridcells);
    buckets.resize(gridcells * bucketsize);
    for (int& c : counters) c = 0;
    usedBuckets.clear();

    bool bucketsTooSmall = false;
    for (size_t i = 0; i < m_spheres.size(); i++)
    {
        int x = (int)((m_spheres[i].pos.x + 0.5f) / gridspacing);
        int y = (int)((m_spheres[i].pos.y + 0.5f) / gridspacing);
        int z = (int)((m_spheres[i].pos.z + 0.5f) / gridspacing);

        int b = (z * gridsize + y) * gridsize + x;
        int n = counters[b];
        if (n < bucketsize) 
        {
            buckets[b * bucketsize + n] = (int)i;
            if (counters[b] == 0) usedBuckets.push_back(b);
            counters[b]++;
        }
        else
        {
            std::cout << "Bucket overflow with bucketsize " << bucketsize << std::endl;
            bucketsTooSmall = true;
        }
    }

    // Gravity forces
	Vec3 gravity = m_gravity;

	std::vector<Vec3> forces(m_spheres.size(), gravity * m_mass);

    // Penalty forces
    nVec3i off[] = {nVec3i(0,0,0)};
    
    for (int b : usedBuckets)
    {
        int x = (b % gridsize);
        int y = (b % (gridsize*gridsize)) / gridsize;
        int z =  b / (gridsize*gridsize);

        for (int w = 0; w < 14; w++)
        {
            int a = (w % 3)     - 1;
            int b = (w % 9) / 3 - 1;
            int c =  w / 9      - 1;

            if (x+a >= 0 && y+b >= 0 && z+c >= 0 &&
                x+a < gridsize && y+b < gridsize && z+c < gridsize)
            {
                int b1 = ( z    * gridsize +  y   ) * gridsize +  x;
                int b2 = ((z+c) * gridsize + (y+b)) * gridsize + (x+a);

                int n1 = counters[b1], n2 = counters[b2];

                for (int i1 = 0; i1 < n1; i1++)
                for (int i2 = 0; i2 < n2; i2++)
                {
                    int j1 = buckets[b1 * bucketsize + i1];
                    int j2 = buckets[b2 * bucketsize + i2];

                    if (w < 13 || j1 < j2)
                    {
                        ComputePairForce(j1, j2, forces);
                    }
                }
            }
        }
    }

    if (bucketsTooSmall) bucketsize++;
        
	// Damping forces
	for (size_t i=0; i<m_spheres.size(); i++)
	{
		Vec3 vel = m_spheres[i].vel;
	
		forces[i] += -m_damping * vel;
	}
    
	return forces;
}

std::vector<Vec3> SphereSystem::ComputeForcesWithKdTree()
{
    KdNode* kdTree = new KdLeaf(); //start with empty leaf
    
    for (const auto& s : m_spheres)
    {
        kdTree = kdTree->AddSphere(&s);
    }

    // Logging of tree stats every 1000 time steps
    static int step = 0;
    step++;
    if (step % 1000 == 0)
    {
        int minHeight = std::numeric_limits<int>::max(), maxHeight = -1, minLeafsize = std::numeric_limits<int>::max(), maxLeafsize = -1;
        kdTree->TreeStats(minHeight, maxHeight, minLeafsize, maxLeafsize, 0);

        std::cout << "minHeight = " << minHeight <<  ", maxHeight = " << maxHeight <<  ", minLeafsize = " << minLeafsize <<  ", maxLeafsize = " << maxLeafsize << std::endl;
    }

    // Gravity forces
	Vec3 gravity = m_gravity;

	std::vector<Vec3> forces(m_spheres.size(), gravity * m_mass);

    // Penalty forces
    std::function<void(KdNode::PSphere,KdNode::PSphere)> onCollision = [this,&forces]
    (const Sphere* si,const Sphere* sj) -> void {
        int i = (int)(si - m_spheres.data());
        int j = (int)(sj - m_spheres.data());
        ComputePairForce(i, j, forces);
    };
    
    KdNode::PairMap finishedPairs;
    kdTree->ResolveCollisions(onCollision, finishedPairs);

    delete kdTree;

	// Damping forces
	for (size_t i=0; i<m_spheres.size(); i++)
	{
		Vec3 vel = m_spheres[i].vel;
	
		forces[i] += -m_damping * vel;
	}

	return forces;
}

void SphereSystem::ComputePairForce(int i, int j, std::vector<Vec3>& forces)
{
    Vec3 pos1 = m_spheres[i].pos;
    Vec3 pos2 = m_spheres[j].pos;

    Vec3 diff = pos2 - pos1;
	
	float distSq = GamePhysics::normNoSqrt(diff);

    float distMax = 2.0f * m_radius;

    if (distSq < distMax*distMax && distSq > 1E-10f)
    {
        float dist = std::sqrt(distSq);

        Vec3 forceDir = diff / dist;
        float forceStrength = m_kernel(dist / distMax);
        forceStrength *= m_forceScale;

        forces[i] -= forceDir * forceStrength;
        forces[j] += forceDir * forceStrength;
    }
}



void SphereSystem::UpdatePositions(float dt)
{
	for (size_t i=0; i<m_spheres.size(); i++)
	{
		{
			Vec3 pos = m_spheres[i].pos;
			Vec3 vel = m_spheres[i].vel;

			pos += dt * vel;

            float off = -0.5f + m_radius;
			for (int f=0; f<6; f++)
			{
				float sign = (f%2==0) ? -1.0f : 1.0f;
				if (sign * pos[f/2] < off) 
				{
					pos[f/2]  = sign * off;
                    vel[f/2] = -vel[f/2];
				}
			}

			m_spheres[i].pos =  pos;
			m_spheres[i].vel =  vel;
		}
	}
}

void SphereSystem::UpdateVelocities(float dt, const std::vector<Vec3>& forces)
{
	for (size_t i=0; i<m_spheres.size(); i++)
	{
		Vec3 vel = m_spheres[i].vel;
		float m = m_mass;

		vel += (dt / m) * forces[i];

		m_spheres[i].vel =  vel;
	}
}
