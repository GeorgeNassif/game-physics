﻿#include "SphereSystemSimulator.h"

std::function<float(float)> SphereSystemSimulator::m_Kernels[5] = {
	[](float x) {return 1.0f; },              // Constant, m_iKernel = 0
	[](float x) {return 1.0f - x; },          // Linear, m_iKernel = 1, as given in the exercise Sheet, x = d/2r
	[](float x) {return (1.0f - x)*(1.0f - x); }, // Quadratic, m_iKernel = 2
	[](float x) {return 1.0f / (x)-1.0f; },     // Weak Electric Charge, m_iKernel = 3
	[](float x) {return 1.0f / (x*x) - 1.0f; },   // Electric Charge, m_iKernel = 4
};

// SphereSystemSimulator member functions
SphereSystemSimulator::SphereSystemSimulator()
{
	m_iTestCase = 1;
	m_fMass         = 0.01f;
	m_fRadius       = 0.03f;
	m_fForceScaling = 1.0f;
	m_fDamping = 0.01f;
	m_iNumSpheres = 25;
	m_iAccelerator = 0;
	m_iKernel = 4;
	m_pSphereSystem = new SphereSystem(m_iNumSpheres, m_fRadius);

}


const char * SphereSystemSimulator::getTestCasesStr()
{
	return "BasicTest";
}

void SphereSystemSimulator::reset()
{
		m_mouse.x = m_mouse.y = 0;
		m_trackmouse.x = m_trackmouse.y = 0;
		m_oldtrackmouse.x = m_oldtrackmouse.y = 0;
}

void SphereSystemSimulator::initUI(DrawingUtilitiesClass * DUC)
{
	this->DUC	 =	DUC;
	//TwAddButton(g_pTweakBar, "Reset Spheres", [](void *) {g_pSphereSystem = std::unique_ptr<SphereSystem>(new SphereSystem(g_iNumSpheres, g_fRadius)); }, nullptr, "");
	TwAddVarCB(DUC->g_pTweakBar, "Num Spheres",TW_TYPE_INT32, 
        [](const void *value, void*  s){((SphereSystemSimulator*) s)->m_pSphereSystem = (new SphereSystem(((SphereSystemSimulator*) s)->m_iNumSpheres = *(int*)value, ((SphereSystemSimulator*) s)->m_fRadius));}, //SetCallback
        [](void *value, void* s ){*(int*)value = ((SphereSystemSimulator*) s)->m_iNumSpheres;}, //GetCallback
        this, "");
    TwAddVarRW(DUC->g_pTweakBar, "Mass",       TW_TYPE_FLOAT,   &m_fMass,         "step=0.001  min=0.001");
    TwAddVarRW(DUC->g_pTweakBar, "Radius",     TW_TYPE_FLOAT,   &m_fRadius,       "step=0.005  min=0.005");
    TwAddVarRW(DUC->g_pTweakBar, "ForceScale", TW_TYPE_FLOAT,   &m_fForceScaling, "step=0.01   min=0");
    TwAddVarRW(DUC->g_pTweakBar, "Damping",    TW_TYPE_FLOAT,   &m_fDamping,      "step=0.001  min=0");
    TwType TW_TYPE_ACCELERATOR = TwDefineEnumFromString("Accelerator", "None (n≤),Grid (m+n),KdTree (nlogn)");
    TwAddVarRW(DUC->g_pTweakBar, "Accelerator", TW_TYPE_ACCELERATOR, &m_iAccelerator, "");
    TwType TW_TYPE_KERNEL = TwDefineEnumFromString("Kernel", "Constant,Linear,Quadratic,WeakElectric,Electric");
	TwAddVarRW(DUC->g_pTweakBar, "Kernel", TW_TYPE_KERNEL, &m_iKernel, "");
}

void SphereSystemSimulator::notifyCaseChanged(int testCase)
{
	m_iTestCase = testCase;
	m_pSphereSystem->clear();
	m_pSphereSystem->setNumberOfParticlesAndRadius(m_iNumSpheres, m_fRadius);
}


void SphereSystemSimulator::externalForcesCalculations(float elapsedTime)
{
	Vec3 pullforce(0, 0, 0);
	Point2D mouseDiff;
	mouseDiff.x = m_trackmouse.x - m_oldtrackmouse.x;
	mouseDiff.y = m_trackmouse.y - m_oldtrackmouse.y;
	if (mouseDiff.x != 0 || mouseDiff.y != 0)
	{
		Mat4 worldViewInv = Mat4(DUC->g_camera.GetWorldMatrix() * DUC->g_camera.GetViewMatrix());
		worldViewInv = worldViewInv.inverse();
		Vec3 forceView = Vec3((float)mouseDiff.x, (float)-mouseDiff.y, 0);
		Vec3 forceWorld = worldViewInv.transformVectorNormal(forceView);
		float forceScale = 0.2f;
		pullforce = pullforce + (forceWorld * forceScale);
	}
	//pullforce -=  pullforce * 5.0f * timeElapsed;

	Mat4 worldInv = Mat4(DUC->g_camera.GetWorldMatrix());
	worldInv = worldInv.inverse();
	Vec3 gravity  = worldInv.transformVectorNormal(Vec3(0, -9.81f, 0));
	externalForce = gravity + pullforce;
}

void SphereSystemSimulator::simulateTimestep(float timeStep)
{
	m_pSphereSystem->SetGravity(externalForce.toDirectXVector());
	m_pSphereSystem->SetMass(m_fMass);
	m_pSphereSystem->SetRadius(m_fRadius);
	m_pSphereSystem->SetForceScale(m_fForceScaling);
	m_pSphereSystem->SetDamping(m_fDamping);
	m_pSphereSystem->SetKernel(m_Kernels[m_iKernel]);
    // Perform one time step of size 'g_fTimestep' per frame
	m_pSphereSystem->AdvanceEuler(timeStep, m_iAccelerator, 10);
	//m_pSphereSystem->AdvanceLeapFrog(timeStep, m_iAccelerator, 10);

}
void SphereSystemSimulator::drawFrame(ID3D11DeviceContext* pd3dImmediateContext)
{

	vector<SphereSystem::Sphere>  spheres = m_pSphereSystem->GetSpheres();
	for (size_t i=0; i<spheres.size(); i++)
    {
		DUC->setUpLighting(Vec3(),0.4*Vec3(1,1,1),100,0.6*Vec3(1,0.92,0.8));
		DUC->drawSphere(Vec3(spheres[i].pos.x, spheres[i].pos.y, spheres[i].pos.z),Vec3(m_fRadius, m_fRadius, m_fRadius));
    }
}


void SphereSystemSimulator::onClick(int x, int y)
{
	m_trackmouse.x = x;
	m_trackmouse.y = y;

}

void SphereSystemSimulator::onMouse(int x, int y)
{
	m_oldtrackmouse.x = x;
	m_oldtrackmouse.y = y;
	m_trackmouse.x = x;
	m_trackmouse.y = y;
}